#include <iostream>
 #include <vector>
 #include <string>
 #include "OnlineVid.h"//include class definition

 using namespace std;

 
 void myfun(OnlineVid views);

 int main()
 {
  OnlineVid d1, d2;//initialized by default c'tor
  OnlineVid d3 (17, 12.10); //initilized  with overloaded c'tor
  OnlineVid d4 = {21, 14}; //different syntax for overloaded c'tor

  //by default we can
  d2  = d3; // default assignment

  //however, by default we cannot print
  //cout<<d1; //error

  myfun(d2);// pass a2 as an argument

    cout<<endl<<"Welcome to class demo..."<<endl;   

    d1.fun();//call the method fun, in object d1    
    //dot operator allows acces to class members



  cout<<"d2.var = "<<d2.getviews()<<endl;
  cout<<"d1.var = "<<d1.getviews()<<endl;
  cout<<"d3.var = "<<d3.getviews()<<endl;
  cout<<"d4.var = "<<d4.getviews()<<endl;

  //  d2.var =5; //error (var is private)

  d2.setviews(5);// correct way

 }//end main

 void myfun(OnlineVid a)
 {
   cout<<"fun with 'a'"<<endl;
   
 }
